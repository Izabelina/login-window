username = ""

while len(username) < 5:
    username = input("Enter your username:  ")
    if len(username) < 5:
        print("Username must be at least 5 characters long")
    continue
else:
    print(username)


password = ""   

while len(password) < 8:
    password = input("Enter your password:  ")
    if (len(password) < 8 and password != password.isalnum()):
        print("Password must be at least 8 characters long and have at least 1 number")
    continue
else:
    print(password)

print("You have logged in.")
