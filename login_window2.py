def logowanie(usernames):

    if_login = False    #dodatkowa zmienna do sprawdzenia, czy zalogowany

    while if_login == False:
        given_username = input("Enter your username:  ")
        given_password = input("Enter your password:  ")
        if given_username not in usernames:
            print("This username is invalid.")
            new_user_names = register(usernames)
            break
        if given_password != usernames[given_username]:
            print("Invalid password.")
            continue
        print("You are logged.")
        if_login = True
    return new_user_names

def register(usernames):
    print("Register as new user.")
    if_new_user = False
    while if_new_user == False:
        new_username = input("Enter your user name:  ")
        if new_username in usernames:
            print("This username is already taken.")
            continue
        while if_new_user == False:
            new_password = input("Enter your password:  ")
            if len(new_password) < 8:
                print("This password is to short.")
                continue
            elif new_password.lower() == new_password:
                print("Password must have 1 upper letter at least.")
                continue
            elif not any(char.isdigit() for char in new_password):
                print("You didn't used a number.")
                continue
            print("Your account is registered correctly.")
            if_new_user = True
    usernames[new_username] = new_password
    return usernames







if __name__ == '__main__':
    usernames = {"Izabela": "jesien11", "Tomek": "lato11", "Magda": "wiosna11"}
    usernames = logowanie(usernames)
